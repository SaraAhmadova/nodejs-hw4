import { readFile, writeFile } from "fs/promises";

interface INews {
  id: string;
  title: string;
  text: string;
}

const getNewsData = async () => {
  try {
    const data = await readFile("./newsData.json", "utf-8");
    return JSON.parse(data);
  } catch (err) {
    console.log("error", err);
  }
};

const writeNewsData = async (news: INews[]) => {
  try {
    await writeFile("./newsData.json", JSON.stringify(news));
  } catch (err) {
    console.error("Error writing news data", err);
  }
};

export { getNewsData, writeNewsData };
